Title: Checking if URL exists with python3
Date: 2018-01-27
Category: tips
Tags: python3, requests
Summary: Simple trick to check if URL exists

I was writing a small program to open my *linux ISO* torrent with HTTP.

My NAS handles downloading with torrent, renames it periodically via cronjob, then serving it over HTTP. So I need to check if the file has been renamed. If it has, get the new filename, else, get the old filename. 
In the requests module, turns out you can only get the HEADER of the reply. This is how:

```
def is_exist(url):
    r = requests.head(url, timeout=1)
    return r.status_code == requests.codes.ok
```

The full script is on `https://gitlab.com/kenzietandun/filebrowser/blob/master/filebrowser.py`
